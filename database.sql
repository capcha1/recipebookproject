CREATE TABLE IF NOT EXISTS ingredient (
	id BIGSERIAL PRIMARY KEY NOT NULL,
	label VARCHAR(255) UNIQUE NOT NULL,
	description VARCHAR(500) NOT NULL,
	energy_value INT,
	protein INT,
	fat INT,
	carbohydrates INT
);

CREATE TABLE IF NOT EXISTS recipe (
    id BIGSERIAL PRIMARY KEY NOT NULL,
    label VARCHAR(255) UNIQUE NOT NULL,
    description VARCHAR(500) NOT NULL,
    cooking_time INT NOT NULL,
    likes INT NOT NULL,
    dislikes INT NOT NULL,
    energy_value INT,
    instruction VARCHAR(3000) NOT NULL,
    views INT,
	image BYTEA
);

CREATE TABLE IF NOT EXISTS commentary (
    id BIGSERIAL PRIMARY KEY NOT NULL,
    create_date DATE NOT NULL,
    change_date DATE NOT NULL,
    content TEXT NOT NULL,
    likes INT NOT NULL,
    dislikes INT NOT NULL
);

CREATE TABLE IF NOT EXISTS recipe_ingredient(
    recipe_ID BIGINT NOT NULL,
    ingredient_ID BIGINT NOT NULL,
    FOREIGN KEY (recipe_ID) REFERENCES recipe(id),
    FOREIGN KEY (ingredient_ID) REFERENCES ingredient(id),
    UNIQUE (recipe_ID, ingredient_ID)
);

CREATE TABLE IF NOT EXISTS recipe_commentary(
    recipe_ID BIGINT NOT NULL,
    commentary_ID BIGINT NOT NULL,
    FOREIGN KEY (recipe_ID) REFERENCES recipe(id),
    FOREIGN KEY (commentary_ID) REFERENCES commentary(id),
    UNIQUE (recipe_ID, commentary_ID)
)

CREATE OR REPLACE FUNCTION bytea_import(P_PATH TEXT, P_RESULT OUT BYTEA) 
                   LANGUAGE PLPGSQL AS $$
DECLARE
  L_OID OID;
BEGIN
  SELECT LO_IMPORT(P_PATH) INTO L_OID;
  SELECT LO_GET(L_OID) INTO P_RESULT;
  PERFORM LO_UNLINK(L_OID);
END;$$;