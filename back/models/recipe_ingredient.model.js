module.exports = (seq, Sequelize) => {
    const recipe_ingredient = seq.define('recipe_ingredient', {
        amount: {
            type: Sequelize.INTEGER
        },
        unit: {
            type: Sequelize.STRING(100)
        },
    },
    {
        timestamps: false,
        freezeTableName: true,
        tableName: 'recipe_ingredient'
    });

    return recipe_ingredient;
};
