const Sequelize = require('sequelize')

const dbConfig = require('../config/db.config')

const seq = new Sequelize(dbConfig.db, dbConfig.username, dbConfig.password, {
    host: dbConfig.host,
    dialect: dbConfig.dialect,
    operatorsAliases: false,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
})

const db = {};

db.Sequelize = Sequelize;
db.seq = seq;

db.recipe = require('./recipe.model.js')(seq, Sequelize);
db.ingredient = require('./ingredient.model.js')(seq, Sequelize);
db.recipe_ingredient = require('./recipe_ingredient.model.js')(seq, Sequelize);

db.recipe.belongsToMany(db.ingredient, {
    through: 'recipe_ingredient',
    foreignKey: 'recipe_id'
});

db.ingredient.belongsToMany(db.recipe, {
    through: 'recipe_ingredient',
    foreignKey: 'ingredient_id'
});

module.exports = db;
