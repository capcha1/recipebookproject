const ingr = require("./ingredient.model");

const Ingredient = ingr.Ingredient;

module.exports = (seq, Sequelize) => {
    const Recipe = seq.define('recipe', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        label: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: {
            type: Sequelize.STRING(500),
            allowNull: false
        },
        cooking_time: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        likes: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        dislikes: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        energy_value: {
            type: Sequelize.INTEGER,
        },
        instruction: {
            type: Sequelize.STRING(3000),
            allowNull: false
        },
        views: {
            type: Sequelize.INTEGER,
        },
        image: {
            type: Sequelize.BLOB
        }
    },
    {
        timestamps: false,
        freezeTableName: true,
        tableName: 'recipe'
    });

    // Recipe.associate = (Ingredient) => {
    //     Recipe.belongsToMany(Ingredient, {
    //         through: 'recipe_ingredient',
    //         foreignKey: 'recipe_id'
    //     });
    // };

    return Recipe;
};
