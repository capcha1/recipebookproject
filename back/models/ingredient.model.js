module.exports = (seq, Sequelize) => {
    const Ingredient = seq.define('ingredient', {
        id: {
            type: Sequelize.BIGINT,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        label: {
            type: Sequelize.STRING,
            allowNull: false
        }
    },
    {
        timestamps: false,
        freezeTableName: true,
        tableName: 'ingredient'
    });

    /*Ingredient.associate = (Recipe) => {
        Ingredient.belongsToMany(Recipe, {
            through: 'recipe_ingredient',
            foreignKey: 'ingredient_id'
        });
    };*/

    return Ingredient;
};
