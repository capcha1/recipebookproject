module.exports = app => {
    const recipes = require("../controller/recipe.controller.js");
    let router = require("express").Router();

    // Create a new Recipe
    router.post("/", recipes.create);

    // Retrieve all Recipes
    router.get("/", recipes.findAll);

    // Retrieve all published Recipes
    //router.get("/published", recipes.findAllPublished);

    // Retrieve a single Recipe with id
    router.get("/:id", recipes.findOne);
    router.get("/find/:query", recipes.findQuery)

    // Update a Recipe with id
    router.put("/:id", recipes.update);

    // Delete a Recipe with id
    router.delete("/:id", recipes.delete);

    // Create a new Recipe
    router.delete("/", recipes.deleteAll);

    app.use('/api/recipe', router);
};
