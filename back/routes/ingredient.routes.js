module.exports = app => {
    const ingredients = require("../controller/ingredient.controller.js");
    let router = require("express").Router();
    let router_find = require("express").Router();

    // Create a new ingredient
    router.post("/", ingredients.create);

    // Retrieve all Ingredients
    router.get("/", ingredients.findAll);

    // Retrieve all published ingredients
    // router.get("/published", recipes.findAllPublished);

    // Retrieve a single ingredient with id
    router.get("/:id", ingredients.findOne);
    router_find.get("/:query", ingredients.queryFind)

    // Update a ingredient with id
    router.put("/:id", ingredients.update);

    // Delete a ingredient with id
    router.delete("/:id", ingredients.delete);

    // Create a new ingredient
    router.delete("/", ingredients.deleteAll);

    app.use('/api/ingredients', router);
    app.use('/api/find', router_find)
};
