const db = require("../models/index");
const Ingredient = db.ingredient;
const Op = db.Sequelize.Op;
const pp = require('pg-promise')();
const conn = `postgres://postgres:123@postgres:5432/recipebookproject`;


// Create and Save a new Ingredient
exports.create = (req, res) => {
    // Validate request
    if (!req.body.title) {
        res.status(400).send({
            message: "Content can not be empty!"
        });

        return;
    }

    // Create a Ingredient
    const Ingredient = {
        id: req.body.id,
        label: req.body.label,
        description: req.body.description,
        energy_value: req.body.energy_value,
        protein: req.body.protein,
        fat: req.body.fat,
        carbohydrates: req.body.carbohydrates,
        image: req.body.image,
    };

    // Save Ingredient in the database
    Ingredient.create(Ingredient)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Ingredient."
            });
        });
};

// Retrieve all Ingredients from the database.
exports.findAll = (req, res) => {
    Ingredient.findAll()
        .then(ingredients => {
            res.send(ingredients)
        })
        .catch(() => {
            res.status(500).send({
                message: "Error retrieving all recipes"
            })
        });
};

exports.queryFind = (req, res) => {
    let q = req.params.query;
    if (q.includes(', '))
        q = q.split(', ');
    else if (q.includes(','))
        q = q.split(',');
    else
        q = q.split(' ');

    console.log(`Find query: %${q}%`);
    let ingredients_id = Array();
    let promises = Array();
    for (let i=0; i<q.length; i++) {
        let query = q[i].slice(1, -1);
        promises.push(Ingredient.findOne({
            where: {
                label: {[Op.like]: `%${query}%`}
            }
        }).then(data => {
            console.log(`Query: %${query}%, Result: ${data}`);
            if (data)
                ingredients_id.push(data.id);
        }).catch(err => {
            res.status(500).send({
                message: "Error for query query=" + q + " error: " + err
            });
        }));
    }
    Promise.all(promises.map(p => p.catch(x => console.error(x)))).then(
        r => {
            console.log(`Финальный промис, параметр: ${r}`);
            if (ingredients_id.length > 0) {
                let q = `WHERE ingredient_id = ${ingredients_id[0]}`;
                console.log(`Прилетел такой массив: ${ingredients_id}`)
                for (let i=1; i < ingredients_id.length; i++)
                    q = `${q} OR ingredient_id = ${ingredients_id[i]}`;
                console.log(`Запрос в базу: ${q}`);

                const database = pp(conn);
                database.any('SELECT id, label, description, cooking_time, energy_value, image FROM recipe_ingredient ' +
                    'INNER JOIN recipe ' +
                    'ON recipe.id = recipe_ingredient.recipe_id ' +
                    `${q};`
                ).then((data) => {
                    console.log(`Запрос отработал. Количество рецептов: ${data.length}`);

                    res.send([...new Set(data)]);
                }).catch((e) => {console.log(`ОШИБКА ЗАПРОСА К БАЗЕ: ${e}`); res.status(500).send({
                    message: "Error while connect to database"
                });}).finally(database.$pool.end);
            }
            else
                res.send();
        }
    ).catch(err => {
        console.log(err);
    })
}

// Find a single Ingredient with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Ingredient.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(() => {
            res.status(500).send({
                message: "Error retrieving Ingredient with id=" + id
            });
        });
};

// Update a Ingredient by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    Ingredient.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num === 1) {
                res.send({
                    message: "Ingredient was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Ingredient with id=${id}. Maybe Ingredient was not found or req.body is empty!`
                });
            }
        })
        .catch(() => {
            res.status(500).send({
                message: "Error updating Ingredient with id=" + id
            });
        });
};

// Delete a Ingredient with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Ingredient.destroy({
        where: {id: id}
    })
        .then(num => {
            if (num === 1) {
                res.send({
                    message: "Ingredient was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Ingredient with id=${id}. Maybe Ingredient was not found!`
                });
            }
        })
        .catch(() => {
            res.status(500).send({
                message: "Could not delete Ingredient with id=" + id
            });
        });
};

// Delete all Ingredients from the database.
exports.deleteAll = (req, res) => {
    Ingredient.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Ingredients were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Ingredients."
            });
        });
};

// Find all published Ingredients
// exports.findAllPublished = (req, res) => {
//
// };
