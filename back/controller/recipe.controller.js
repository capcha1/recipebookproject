const { recipe_ingredient } = require("../models/index");
const db = require("../models/index");

const Recipe = db.recipe;
const Ingredient = db.ingredient;
const Op = db.Sequelize.Op;

// Create and Save a new Recipe
exports.create = (req, res) => {
    console.log("CREATE RECIPE REQUEST");
    // Validate request
    if (!req.body.label) {
        res.status(400).send({
            message: "Content can not be empty!"
        });

        return;
    }

    console.log(req.body);

    // Create a Recipe
    const recipe = {
        label: req.body.label,
        description: req.body.description,
        cooking_time: req.body.cooking_time,
        likes: 0,
        dislikes: 0,
        energy_value: req.body.energy_value,
        instruction: req.body.instruction,
        views: 0,
        image: req.body.image
    };

    const ingredients = req.body.ingredients;
    // Save Recipe in the database
    Recipe.create(recipe)
        .then((data) => {
            let rec_id = data.dataValues.id;
            if (ingredients)
            {
                ingredients.map(ingr => {
                    const ingredient = {label: ingr.label};
                    
                    Ingredient.create(ingredient)
                        .then(data => {
                            let ingr_id = data.dataValues.id;
                            const rec_ingr = {
                                recipe_id: rec_id,
                                ingredient_id: ingr_id,
                                amount: ingr.recipe_ingredient.amount,
                                unit: ingr.recipe_ingredient.unit
                            };
        
                            recipe_ingredient.create(rec_ingr)
                                .then(data => { })
                                .catch(err => {
                                    res.status(500).send();
                                });
                        })
                        .catch(err => {
                            res.status(500).send();
                        })
                })

                res.status(200).send();
            }
        })
        .catch(err => {
            res.status(500).send();
        });
};

// Retrieve all Recipes from the database.
exports.findAll = (req, res) => {
    console.log("GET ALL RECIPES REQUEST");
    Recipe.findAll()
        .then(recipes => {
            res.send(recipes)
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving all recipes"
            })
        });
};

// Find a single Recipe with an id
exports.findOne = (req, res) => {
    const _id = req.params.id;
    console.log(_id);
    console.log("GET ONE RECIPE REQUEST");

    Recipe.findByPk(_id, {
        include: [
          {
            model: Ingredient,
            attributes: ["id", "label"],
            through: {
              attributes: ["amount", "unit"]
            }
          }
        ],
      }).then(rec => {
          console.log(rec);
          rec.dataValues.instruction = rec.dataValues.instruction.split(":::");
          console.log(rec);
          return res.send(rec);
      })
      .catch(err => {
            res.status(500).send({
                message: "Error retrieving Recipe with id=" + _id
            });
        });
};

exports.findQuery = (req, res) => {
    q = req.params.query.slice(1);
    console.log(`Find query: ${q}`);
    Recipe.findAll({
        where: {
            label: {[Op.like]: `%${q}%`}
        }
    }).then(data => {
        console.log(`Result: ${data}`);
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: "Error for query query=" + req + " error: " + err
        });
    });
}

// Update a Recipe by the id in the request
exports.update = (req, res) => {
    console.log("UPDATE RECIPE REQUEST");
    const id = req.params.id;

    Recipe.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num === 1) {
                res.send({
                    message: "Recipe was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Recipe with id=${id}. Maybe Recipe was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Recipe with id=" + id
            });
        });
};

// Delete a Recipe with the specified id in the request
exports.delete = (req, res) => {
    console.log("DELETE RECIPE REQUEST");
    const id = req.params.id;

    Recipe.destroy({
        where: {id: id}
    })
        .then(num => {
            if (num === 1) {
                res.send({
                    message: "Recipe was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Recipe with id=${id}. Maybe Recipe was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Recipe with id=" + id
            });
        });
};

// Delete all Recipes from the database.
exports.deleteAll = (req, res) => {
    console.log("DELETE ALL RECIPES REQUEST");
    Recipe.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Recipes were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all recipes."
            });
        });
};
// Find all published Recipes
// exports.findAllPublished = (req, res) => {
//
// };
