module.exports = {
    // params for PG connection
    host: "postgres",
    username: "postgres",
    password: "123",
    db: "recipebookproject",
    dialect: "postgres",

    // params for Sequelize connection pool
    pool: {
        min: 0, // minimum number of connections in pool
        max: 5, // maximum number of connections in pool
        acquire: 30000, // maximum time, in milliseconds, that pool will try to get connection before throwing error
        idle: 10000, // maximum time, in milliseconds, that a connection can be idle before being released
    }
}
