var express = require('express');
const bodyParser = require("body-parser");
const cors = require('cors');


var app = express();


//cors provides Express middleware to enable CORS with various option
let corsOptions = {
 "Access-Control-Allow-Origin" : "*",
 "Access-Control-Allow-Methods" : "GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS",
 "charset" : "utf8"
};

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.json({limit: '100mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '100mb', extended: true}))

app.use(cors(corsOptions));

const db = require('./models/index');
db.seq.sync();

require('./routes/recipe.routes')(app);
require('./routes/ingredient.routes')(app);

// set port, listen for requests
const PORT = 8070;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

module.exports = app;


// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'pug');

// app.use(logger('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', indexRouter);
// app.use('/users', usersRouter);

// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });

// // error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

// module.exports = app;
