# Книга рецептов

|  Name                 |  Gitlab Username  |
| -------------         |:-------------:    |
| Ртищева Ксения        | @krtisheva        |
| Бортникова Анна       | @AnnBortnikova    |
| Карасюк Артем         | @Polisika         |
| Милютин Василий       | @Wasilkas         |
| Касимов Тимур         | @SN4KEBYTE        |
| Фадейкин Леонид       | @capcha1          |  

## Макет сайта

![mainpage](mainpage.jpg)

## Рецепт

![recipe](recipe.png)

## Создание рецепт

![create_recipe](create_recipe.png)
