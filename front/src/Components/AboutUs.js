import React, {Component} from 'react'
import {Container} from "react-bootstrap"

export default class AboutUs extends Component {
    render() {
        return (
            <Container>
                <div className="border border-dark mt-5 mb-5">
                    <blockquote className="blockquote text-center">
                        <h2 className="mt-5 mb-5">О разработчиках</h2>
                    </blockquote>

                    <div className="row">
                        <div className="col ml-5">
                            <h4 className="ml-5 mb-3">Бортникова Анна</h4>
                            <h4 className="ml-5 mb-3">Карась Артем</h4>
                            <h4 className="ml-5 mb-3">Касимов Тимур</h4>
                            <h4 className="ml-5 mb-3">Милютин Василий</h4>
                            <h4 className="ml-5 mb-3">Ртищева Ксения</h4>
                            <h4 className="ml-5 mb-5">Фадейкин Леонид</h4>
                        </div>
                        <div className="col text-center">
                            <h4 className="mt-5 mb-5">Почта для обратной связи:</h4>
                            <h4 className="mt-5 mb-5">recipecooking.space@gmail.com</h4>
                        </div>
                    </div>
                </div>
            </Container>
        )
    }
}
