import React, {useEffect, useState} from 'react'
import tom from '../images/fork.png';
import Recipe from './Recipe/Recipe.js';
import {Container} from "react-bootstrap";
import url from './consts';

const ListRecipe = () => {
    const [recipes, setRecipes] = useState([]);

    const getRecipes = async () => {
        try {
            console.log("Try get recipes from backend")
            const response = await fetch(`${url}/api/recipe/`,
                {headers: {"Access-Control-Allow-Origin": "*"}});
            console.log("Succesfully, response:")
            console.log(response);
            const jsonData = await response.json();
            console.log(`Прилетело: ${jsonData}`);
            setRecipes(jsonData);
        } catch (err) {
            console.log("Error occured");
            console.error(err.message);
        }
    };

    useEffect(() => {
        getRecipes();
    }, []);

    return (
        <Container>
            <div className="scroll border border-dark mt-5">
                {recipes.map((item, index) => (
                    <Recipe
                        key={index.toString()}
                        id={item.id}
                        image={tom}
                        label={item.label}
                        description={item.description}
                        cooking_time={item.cooking_time}
                        energy_value={item.energy_value}
                    />
                ))}
            </div>
        </Container>
    );
};

export default ListRecipe;
