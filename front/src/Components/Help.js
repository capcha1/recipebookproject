import React, {Component} from 'react';
import {Container} from "react-bootstrap";

export default class Help extends Component {
    render() {
        return (
            <Container>
                <div className="border border-dark mt-5 mb-5">
                    <blockquote className="blockquote text-center">
                        <h2 className="mt-5 mb-5">Помощь</h2>
                    </blockquote>

                    <div>
                        <h4 className="ml-5 mb-3">Регистрация:</h4>
                        <div className="ml-5 mb-4 mr-5" align="justify">
                            <p className="mb-1">Если Вам не пришло письмо с подтверждением регистрации, то есть несколько причин почему такое могло случиться.</p>
                            <p className="mb-1">Первая причина - письмо попало в спам, следовательно, нужно проверить в своей почте спам.</p>
                            <p className="mb-1">Вторая причина - Вы неправильно ввели свой email при заполнении формы регистрации. 
                                Тогда нужно будет проверить свои заполненные данные.</p>
                            <p className="mb-1">Если возникли вопросы по регистрации, можно написать нам на почту recipecooking.space@gmail.com.</p>
                        </div>
                    </div>

                    <div>
                        <h4 className="ml-5 mb-3">Правило модерации:</h4>
                        <div className="ml-5 mb-4 mr-5" align="justify">
                            <p className="mb-1">По вопросам одобрения Вашего рецепта, просьба обращаться по почте recipecooking.space@gmail.com.</p>
                            <p>Главное правило, чтобы Ваш рецепт был опубликован 
                                - это то, что материал должен быть на кулинарную тему 
                                или тема должна касаться кухни или питания для здоровья. </p>
                        </div>
                    </div>
                    
                    <div>
                        <h4 className="ml-5 mb-3">Снятие с публикации рецепта:</h4>
                        <div className="ml-5 mb-1 mr-5" align="justify">
                            <p className="mb-1">Разработчика сайта вводят запрет на публикацию рецепта, авторские права
                                на которые не принадлежат тем, кто их размещает. То есть за разработчиками сохраняется право снимать с публикации любой рецепт, 
                                в котором используются фотографии или текст, скопированные с других сайтов или журналов, не принадлежащие автору рецепта.</p>
                            <p className="mb-1">Если вы где-то прочитали какой-либо рецепт, то Вы можете его добавить на сайт при условии, 
                                что приготовив его самостоятельно, напишите текст, как Вы его готовили, и сделайте свои фотографии.</p>
                        </div>
                    </div>
                    
                    <div>
                        <h4 className="mt-3 ml-5 mb-3">Правила добавления рецепта:</h4>
                        <ol className="ml-4 mb-1 mr-5" align="justify">
                            <li>...</li>
                        </ol>
                    </div>

                </div>
            </Container>
        )
    }
}