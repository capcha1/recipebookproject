import React, { Component } from 'react'
import './Recipe.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link} from "react-router-dom";

export default class Recipe extends Component {
    render() {
        return (
            <div className="border border-dark m-5">
                <div className="row mt-5 mb-5">
                    <Link to={`/${this.props.id}`} className="text-dark text-decoration-none">
                        <img
                            className="img"
                            width="30%"
                            src={this.props.image}
                            alt="Tom yum soup"
                            align="left"
                            hspace="60"
                        />
                        <h3 className="head">{this.props.label}</h3>
                        <p className="mt-3">{this.props.description}</p>
                        <div className="dataRecipe">
                            <div>
                                <h5 className="subtitle mr-2">Время приготовления:</h5>
                                <p className="data mr-3">{this.props.cooking_time} мин</p>
                            </div>
                            <div>
                                <h5  className="subtitle mr-2">Калорийность:</h5>
                                <p className="data" >{this.props.energy_value} ккал/100 г</p>
                            </div>
                        </div>
                    </Link>
                </div>
            </div>
        )
    }
}
