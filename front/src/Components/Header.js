import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import {Container} from "react-bootstrap";
import {Link} from "react-router-dom";

export default class Header extends Component {
    render() {
        return (
            <Container>
                <Link to={"/"} className="text-dark text-decoration-none">
                    <div>
                        <h1 className="display-4 font-weight-bold text-center mt-5">КНИГА РЕЦЕПТОВ</h1>
                    </div>
                </Link>
            </Container>
        );
    }
}