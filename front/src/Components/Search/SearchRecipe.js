import React, {useState, useEffect} from 'react';
import { Container } from 'react-bootstrap';
import {useParams} from "react-router-dom";
import tom from '../../images/tom-yam.jpg';
import Recipe from '../Recipe/Recipe'
import './Search.css'
import url from '../consts'

const Search = () => {
    let params = useParams();
    const [recipes, setRecipes] = useState([]);

    const getRecipe = async () => {
        try {
            console.log("Send request")
            console.log(params.line)
            const response = await fetch(`${url}/api/recipe/find/${params.line}`,
                {headers: {"Access-Control-Allow-Origin": '*'}});
            const jsonData = await response.json();
            console.log("Succesfully: " + JSON.stringify(jsonData))
            setRecipes(jsonData);
        } catch (err) {
            console.error(err.message);
        }
    };

    useEffect(() => {
        getRecipe();
    }, []);

    console.log(recipes);

    return (
        <Container>
            <div className="scroll border border-dark mt-5">
                {recipes.length !== 0 ? recipes.map((item, index) => (
                    <Recipe
                        key={index.toString()}
                        id={item.id}
                        image={tom}
                        label={item.label}
                        description={item.description}
                        cooking_time={item.cooking_time}
                        energy_value={item.energy_value}
                    />
                )) : <h2 className="text-center m-5 p-5" align="center">Нет таких рецептов!</h2>}
            </div>
        </Container>
    );
}
export default Search;
