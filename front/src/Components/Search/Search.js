import React, {useState} from 'react';
import { Container } from 'react-bootstrap';
import {Link} from "react-router-dom";
import './Search.css'

const Search = () => {
    const [line, setLine] = useState("");

    return (
        <Container>
            <form className="inputWithButton mt-5" >
                <input
                    type="text"
                    className="form-control form-control-lg"
                    placeholder="поиск рецептов"
                    onChange={event => (setLine(event.target.value), console.log(line))}
                />
                <Link to={`/search/${line}`} className="text-dark text-decoration-none">
                    <button type="submit">Найти</button>
                </Link>
            </form>
        </Container>
    );
}
export default Search;