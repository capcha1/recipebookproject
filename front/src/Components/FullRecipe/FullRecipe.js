import React, {useEffect, useState} from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import {useParams, Link} from "react-router-dom";
import './fullRecipe.css'
import tom from '../../images/salad.png';
import url from '../consts'

const FullRecipe = () => {

    let params = useParams();
    let instructions = ["",""];
    let [recipe, setRecipe] = useState({
        label: "",
        instruction: "", 
        ingredients:[{
            label: "",
            recipe_ingredient: {
                amount: "",
                unit: ""
            }
        }]
    });

    const deleteRecipe = async () => {
        try {
            const response = await fetch(`${url}/api/recipe/${params.id}`,{
            method: "DELETE",  
            headers: {"Access-Control-Allow-Origin": "*"}});
            console.log(response);
        } catch (err) {
            console.error(err.message);
        }
    };

    const getRecipe = async () => {
        try {
            console.log("Send request")
            const response = await fetch(`${url}/api/recipe/${params.id}`,
                {headers: {"Access-Control-Allow-Origin": '*'}});
            const jsonData = await response.json();
            console.log("Succesfully: " + JSON.stringify(jsonData))
            jsonData.instruction = jsonData.instruction.slice(1);
            setRecipe(jsonData);
        } catch (err) {
            console.error(err.message);
        }
    };

    useEffect(() => {
        getRecipe();
    }, []);

    console.log(recipe);

    return (
        <Container>
            <div className="border border-dark mt-5 mb-5">
                <div className="text-right">
                <Link to={"/"} className="text-dark text-decoration-none">
                    <button type="submit" className="border border-dark btn btn-light mt-3 mr-3"
                        onClick={()=>{ deleteRecipe() }}>Удалить рецепт!</button>
                </Link>
                </div>
                <div>
                    <blockquote className="blockquote text-center">
                        <h2>{recipe.label}</h2>
                    </blockquote>
                    <Row className=" transform: translateY(39%);">
                        <img
                            className="image mt-5 mr-4"
                            height="50%"
                            width="50%"
                            src={tom}
                            alt="Tom yum soup"
                            align="left"
                            hspace="60"
                        />
                        <div className="col-5 mt-3">
                            <h4 className="mb-3" align="center">Ингредиенты</h4>
                            {recipe.ingredients.map((item, index) => (
                                <Row className="products row" key={index.toString()}>
                                    <Col>
                                            <h5 className="product mb-2">{item.label}</h5>
                                    </Col>
                                    <Col>
                                            <p className="quantity mb-2 text-right">{item.recipe_ingredient.amount} {item.recipe_ingredient.unit}</p>
                                    </Col>
                                </Row>
                            ))}
                        </div>
                    </Row>
                </div>
                <Row className="ml-5">
                    <h5 className="subtitle">Время приготовления:</h5>
                    <p className="data ml-3">{recipe.cooking_time} мин</p>

                    <h5  className="subtitle ml-5">Калорийность:</h5>
                    <p className="data ml-3" >{recipe.energy_value} ккал/100 г</p>
                </Row>
                {
                <div className="step mx-auto">
                    <h4 className="mt-5 mb-4 text-center">Пошаговое приготовление:</h4>
                    { 
                        instructions = recipe.instruction.split(":::"),
                        console.log(`${instructions}`),
                        instructions.map((item, index) => (
                            <div key={index.toString()}>
                                <h5>Шаг {index+1}:</h5>
                                <p>{item}</p>
                            </div>
                    ))}
                </div>
                }
                <blockquote className="blockquote text-center">
                        <h2 className="mt-4 mb-5">Приятного аппетита!</h2>
                </blockquote>
            </div>
        </Container>
    )
}

export default FullRecipe;
