import React, { useEffect, useRef, useState } from 'react';
import "./photo.css"
import icon from "../../../images/icon.png"
import cross from "../../../images/cross_img.png"
let file_img = undefined;
export default function Photo(props) {

    const [preview, setPreview] = useState();
    const fileInputRef = useRef();

    // useEffect(() => {
    //     console.log("Я сработал");
    //     if (file_img) {
    //         const reader = new FileReader();
    //         reader.onloadend = () => {
    //             setPreview(reader.result);
    //         };
    //         reader.readAsDataURL(file_img);
    //         console.log(`Прилетело: ${preview}`);
    //         props.setImage(preview);
    //     } else {
    //         setPreview(null);
    //     }
    // }, [props.image]);

    return (
        <div className="border border-dark" id="choose_img">
            <form>
               {preview ? ( 
                    <div>
                        <img
                            src={cross} 
                            alt="icon" 
                            width="1.5%" 
                            align="right"  
                            id="cross_img"
                            onClick={() => { props.setImage(null); }}
                        /> 
                        <div>
                            <img src={preview} id="your_img"/>
                        </div> 
                            
                    </div>
                    ) : (
                        <button id="button_img" 
                            onClick={(event) => {
                                event.preventDefault();
                                fileInputRef.current.click();
                            }}>
                            <div className="icon_drop">
                                <img src={icon} alt="icon" width="25%" />
                            </div>
                            
                        </button>
                    )}
                <input type="file" 
                    id="photo" 
                    accept="image/*" 
                    className="form-control-file ml-5 mb-5" 
                    placeholder="фото рецепта"
                    ref={fileInputRef}
                    onChange={(event) => {
                        const file = event.target.files[0];
                        if (file && file.type.substr(0, 5) === "image") {
                            console.log("Получили файл");
                            const reader = new FileReader();
                            reader.onloadend = () => {
                                let res = reader.result
                                setPreview(res);
                                props.setImage(res);
                            };
                            reader.readAsDataURL(file);
                        } else {
                            props.setImage(null);
                        }
                    }}
                />  
             </form>
        </div>
    );
}
