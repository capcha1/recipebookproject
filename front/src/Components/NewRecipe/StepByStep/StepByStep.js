import React, {useState} from 'react'
import {Col, Row} from "react-bootstrap";
import "./stepByStep.css"
import Photo from "../Photo/Photo"
import cross from "../../../images/cross1.png"
import plus from "../../../images/plus.png"

let num = 2;
let step_text = [];
let photos = [];


const StepByStep = (props) => {
    const get_id = () => Math.random().toString(36).substr(2,9);
    const [items, setItems] = useState([{
        id: get_id(),
        nameIng: '',
        num: 1}])
  
    const addStep= () => {
        const StepDate = {
            id: get_id(),
            nameIng: '',
            num: num
        }
        num++;
        let add = setItems([...items, StepDate]);
        console.log(add);
        setItems([...items, StepDate])
    };
    
    const removeStep = (num_array) => {
        let res = items.filter((item) => item.num !== num_array);
        if (num !== 2) {
            num--;
            for (let i = 0; i < res.length; i++)
                res[i].num = i + 1;
            for (let i = num_array; i < step_text.length - 1; i++)
                step_text[i] = step_text[i + 1];
        }
        else {
            res = [{num: 1, id: get_id(), nameIng: ''}];
        }
        step_text.pop();
        photos[num_array] = undefined;
        props.setPhotos(photos.slice(1));
        setItems(res);
        let string_instruction = step_text.slice(1).join(' ::: ');
        console.log(string_instruction);
        props.setInstruction(string_instruction);
    };

    const handleChangeName = (e, id) => {
        let index = parseInt(id);
        step_text[index] = e.currentTarget.value;
        console.log("Зашли обновлять инструкцию");
        let res = step_text.slice(1).join(' ::: ');
        props.setInstruction(res);
        console.log(`${res} - вот наш результат инструкции`);
    };

    const steps = items.map((item) => {
     
        let textElement = step_text[item.num];

        return (
            <div key={item.id} className="border border-dark ml-5 mr-5 mt-3 mb-5 p-3">
                <Row style={{width: "109%"}} >
                    <Col>
                        <h4 className="head">Шаг {item.num}:</h4>
                        <textarea
                            value={textElement}
                            onChange={(e) => handleChangeName(e, item.num)}
                            className="form-control-lg mb-5" id="text" 
                            placeholder="описание шага" />

                        {/*<h4 className="head">Добавьте фотографию для шага {item.num}:</h4>*/}
                        {/*<Photo photosArray={photos} itemNum={item.num} />*/}
                    </Col>
                    
                    <Col className="col-md-1 offset-md-1 ml-4" id="cross_plus">
                        <div onClick={() => removeStep(item.num)} className="cross">
                            <img src={cross} style={{width: "16px"}} alt='x'/>
                        </div>

                        <div onClick={addStep}>
                            <img src={plus} className="mb-lg-5" style={{width: "18px"}} alt='+'/>
                        </div>
                    </Col>
                </Row>      
            </div>
      );
    });

    return (
        <>
            {steps}
        </>
    );

};

export default StepByStep;
