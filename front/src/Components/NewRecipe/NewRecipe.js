import React, { useState } from 'react';
import {Col, Container, Row} from "react-bootstrap";
import "./NewRecipe.css"
import Photo from "./Photo/Photo"
import CreateIngredients from "./CreateIngredients/CreateIngredients"
import StepByStep from "./StepByStep/StepByStep"
import url from '../consts'

const NewRecipe = () => {
    const postRecipe = async (data) => {
        try {
            const response = await fetch(`${url}/api/recipe`,{
            method: "POST",  
            body: JSON.stringify(data),
            headers: {"Content-Type": "application/json", "Access-Control-Allow-Origin": '*'}});
            console.log(response);
        } catch (err) {
            console.error(err.message);
        }
    };

    const [label_recipe, setLabel_recipe] = useState('');
    const [description, setDescription] = useState('');
    const [cooking_time, setCooking_time] = useState('');
    const [energy_value, setEnergy_value] = useState('');
    const [instruction, setInstruction] = useState('');
    const [photos, setPhotos] = useState([]);
    const [photo, setPhoto] = useState('');

    const [label_ingr, setLabel_ingr] = useState([]);
    const [amount_ingr, setAmount_ingr] = useState([]);
    const [unit_ingr, setUnit_ingr] = useState([]);
    // здесь хранится мусор, используется для обновление трёх верхних переменных
    const [fictive, set_fictive] = useState(2);

    return (
        <Container>
            <div className="border border-dark mt-5 mb-5">
                <blockquote className="blockquote text-center">
                    <h2 className="mt-5">Добавление своего рецепта</h2>
                </blockquote>
                <form className="mt-5">
                    <h4 className="head ml-5">Введите название рецепта:</h4>
                    <input type="text" id="name" className="form-control-lg ml-5 mb-5"
                        placeholder="название рецепта" ref={ref => setLabel_recipe(ref)} />

                    <h4 className="head ml-5">Добавьте фотографию блюда:</h4>
                    <div className="ml-5">
                        <Photo setImage={setPhoto} />
                    </div>
                    
                    <h4 className="head ml-5 mt-5">Добавьте краткое описание Вашего блюда:</h4>
                    <textarea className="form-control-lg ml-5 mb-5" id="text" placeholder="описание блюда" 
                        ref={ref => setDescription(ref)} />

                    <Row>
                        <Col>
                            <h4 className="head ml-5">Время приготовления:</h4>
                            <div className="input-group-append">
                                <input type="text" id="num" className="form-control-lg ml-5 mb-5" placeholder="10" 
                                    ref={ref => setCooking_time(ref)} />
                                <span id="units" className="input-group-text ml-2">мин</span>
                            </div>
                        </Col>
                        <Col>
                            <h4 className="head ml-5">Калорийность:</h4>
                            <div className="input-group-append">
                                <input type="text" id="num" className="form-control-lg ml-5 mb-5" placeholder="300" 
                                    ref={ref => setEnergy_value(ref)} />
                                <span id="units" className="input-group-text ml-2">ккал / 100 гр</span>
                            </div>
                        </Col>
                    </Row>

                    <h4 className="head ml-5">Ингредиенты:</h4>
                    <CreateIngredients setAmount={setAmount_ingr} setUnit={setUnit_ingr} setLabel={setLabel_ingr} setFictive={set_fictive}/>

                    <h4 className="head ml-5">Пошаговое приготовление:</h4>
                    <StepByStep setInstruction={setInstruction} setPhotos={setPhotos}/>

                    <button type="submit" className="border border-dark btn btn-light ml-5" onClick={()=>{
                        let ingredients = Array();
                        for (let i = 0; i < label_ingr.length; i++)
                        {
                            let buff = {
                                "label": `${label_ingr[i]}`,
                                "recipe_ingredient":{
                                    "amount": `${amount_ingr[i]}`,
                                    "unit": `${unit_ingr[i]}`
                                }
                            };
                            console.log(`${buff} - ${i}-й ингредиент`);
                            ingredients.push(buff);
                        }
                        let data = {
                            "label": `${label_recipe.value}`,
                            "description": `${description.value}`,
                            "instruction": `${instruction}`,
                            "cooking_time": `${cooking_time.value}`,
                            "energy_value": `${energy_value.value}`,
                            "ingredients": ingredients,
                            "image": photo,
                        }
                        postRecipe(data).then(res => console.log(res))
                    }}>Отправить рецепт!</button>
                    <small className="form-text text-muted ml-5 mb-5">Нажимая на кнопку "Отправить рецепт",
                        Вы отправляете свой рецепт на модерацию.</small>
                </form>
            </div>
        </Container>
    );
}

export default NewRecipe;
