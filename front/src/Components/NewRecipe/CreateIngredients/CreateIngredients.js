import React, {useState} from 'react'
import {Col, Row} from "react-bootstrap";
import "./createIngredients.css"
import cross from "../../../images/cross1.png"
import plus from "../../../images/plus.png"
let num = 1;
let names = [];
let counts = [];
let units_ = [];

const CreateIngredients = (props) => {
    const get_id = () => Math.random().toString(36).substr(2,9);
    const units = ["шт.", "мл.", "л.", "гр.", "кг.", "ст.л.", "ч.л.", "зубч.", "стакан", "по вкусу"];

    const [state, setState] = useState([{id: get_id(),
        nameIng: '',
        num: 0}])

    const addIngredients = () => {
        const IngredientDate = {
            id: get_id(),
            nameIng: '',
            num: num
        }
        num++;
        let add = setState([...state, IngredientDate]);
        console.log(add);
        setState([...state, IngredientDate])
    };

    const handleChangeName = (e, id) => {
        let index = parseInt(id);
        names[index] = e.currentTarget.value;
        setState(state);
    };
    const handleChangeCount = (e, id) => {
        let index = parseInt(id);
        counts[index] = e.currentTarget.value;
        setState(state);
    };
    const removeIngredients = (num_array) => {
        num--;
        let res = state.filter((item) => item.num !== num_array);
        names[num_array] = units_[num_array] = counts[num_array] = undefined;
        if (num === 0)
            res = [{id: get_id(),
                nameIng: '',
                num: num++}];
        setState(res);
    };

    const ingredient = state.map((item) => {
        props.setAmount(counts);
        props.setLabel(names);
        props.setUnit(units_);

        let array_index = item.num;
        let name = names[array_index], count = counts[array_index];
        return (
            <>
                <div key={item.id} className="border border-dark ml-5 mr-5 mt-3 mb-5 p-3">
                    <Row style={{width: "102%"}} >
                        <Col>
                            <h4 className="head">Введите название ингредиента:</h4>
                            <input type="text" id="nameIng"
                                   onChange={(e) => handleChangeName(e, item.num)}
                                   value={name} className="form-control-lg"
                                   placeholder="название ингредиента" ref={ref => {props.setFictive(ref + 1)}} />
                        </Col>

                        <Col>
                            <h4 className="head ml-5">Кол-во:</h4>
                            <input type="text" id="num"
                                   onChange={(e) => handleChangeCount(e, item.num)}
                                   value={count} className="form-control-lg ml-5" placeholder="10"/>
                            <select className="form-control-lg ml-2" id="select" ref={ref => { if (ref) units_[array_index] = ref.value}} >
                                {units.map((item, index) => (<option key={index.toString()}>{item}</option>))}
                            </select>
                        </Col>

                        <Col className="col-auto" id="cross_plus">

                            <div onClick={() => removeIngredients(item.num)} className="mb-5">
                                <img src={cross} style={{width: "16px"}} alt='x'/>
                            </div>

                            <div onClick={addIngredients}>
                                <img src={plus} style={{width: "18px"}} alt='+'/>
                            </div>

                        </Col>
                    </Row>
                </div>
            </>
        );
    });

    return (
        <>
            {ingredient}
        </>
    );

};

export default CreateIngredients;

        
