import React from 'react';
import './menu.css'
import '../Hamburger/Hamburger'
import {Link} from "react-router-dom";

const Menu = ({items, active, setActive}) => {
   return (
       <div className={active ? 'menu active' : 'menu'} onClick={() => setActive(false)}>
        <div className="blur"/> 
           <div className="content" >
                <ul className="designation mt-3">
                    {items.map((item, index) =>
                        <li className="designation_list text-center" key={index.toString()}>
                            <Link to={item.href} className="text-dark text-decoration-none">{item.value}</Link>
                        </li>
                    )}
                </ul>
           </div>
       </div>
   );
};

export default Menu;
