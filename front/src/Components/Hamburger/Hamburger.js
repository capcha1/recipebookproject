import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import './hamburger.css'
import Menu from '../Menu/Menu'

const Hamburger = () => {
    const items = [{value:"Создать свой рецепт", href: '/createRecipe'}, {value:"О разработчиках", href: '/aboutDevelopers'}, {value:"Помощь", href: '/help'}]
    const [menuActive, setMenuActive] = useState(false);
    const [status, setStatus] = useState('close');
    return (
        <>
             <div
                className="Hamburger_container fixed-top mt-3 ml-3"
                role="button"
                onClick={() => setMenuActive(!menuActive ) & setStatus(status === 'open' ? 'close' : 'open')}
            >
                <i id="burger" className={status}/>
                <i id="burger"  className={status}/>
                <i id="burger"  className={status}/>
                <Menu items={items} active={menuActive} setActive={setMenuActive} />
                
            </div>
        </>
    );
};

export default Hamburger;
