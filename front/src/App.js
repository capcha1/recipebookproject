import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import Hamburger from "./Components/Hamburger/Hamburger";
import Header from "./Components/Header";
import Search from "./Components/Search/Search";
import ListRecipe from "./Components/ListRecipe";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import FullRecipe from "./Components/FullRecipe/FullRecipe"
import NewRecipe from "./Components/NewRecipe/NewRecipe"
import AboutUs from "./Components/AboutUs";
import Help from "./Components/Help";
import SearchRecipe from "./Components/Search/SearchRecipe"

function App() {
  return (
      <div>
          <Router>
              <Hamburger/>
              <Header/>
              <Search/>
              <Switch>
                  <Route exact path="/" component={ListRecipe}/>
                  <Route exact path="/createRecipe" component={NewRecipe}/>
                  <Route exact path="/aboutDevelopers" component={AboutUs}/>
                  <Route exact path="/help" component={Help}/>
                  <Route exact path="/:id" component={FullRecipe}/>
                  <Route exact path="/search/:line" component={SearchRecipe}/>
              </Switch>
          </Router>
      </div>
  );
}

export default App;
